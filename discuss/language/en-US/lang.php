<?php
/* @authorcode  codestrings
 * @copyright   Leyun internet Technology(Shanghai)Co.,Ltd
 * @license     http://www.dzzoffice.com/licenses/license.txt
 * @package     DzzOffice
 * @link        http://www.dzzoffice.com
 * @author      zyx(zyx@dzz.cc)
 */
if(!defined('IN_DZZ')) {
	exit('Access Denied');
}

$lang = array
(

	'EDT' => 'edit',
  'do_success'=> 'please wait',
	'DEL' => 'delete',
	'DLP' => 'delete reply',
	'DCM' => 'delete comments',
	'PRN' => 'batch deletion',
	'UDL' => 'undelete',

	'DIG' => 'add essence',
	'UDG' => 'remove essence',
	'EDI' => 'limited essence',
	'UED' => 'remove limited essence',

	'CLS' => 'close',
	'OPN' => 'open',
	'ECL' => 'limited closing',
	'UEC' => 'remove limited closing',
	'EOP' => 'limited opening',
	'UEO' => 'remove limited opening',

	'STK' => 'topping',
	'UST' => 'un top',
	'EST' => 'limited topping',
	'UES' => 'remove limited topping',

	'SPL' => 'division',
	'MRG' => 'merge',

	'HLT' => 'highlight',
	'UHL' => 'Unhighlight',
	'EHL' => 'limited highlight',
	'UEH' => 'remove limited highlight',

	'BMP' => 'promote',
	'DWN' => 'sink',

	'MOV' => 'move',
	'CPY' => 'copy',
	'TYP' => 'classification',

	'RFD' => 'compulsory refund',

	'MOD' => 'approved',

	'ABL' => 'join corpus',
	'RBL' => 'remove Collection',

	'PTS' => 'push topic',
	'RFS' => 'unpush',
	'RMR' => 'cancel reward',
	'BNP' => 'block posts',
	'UBN' => 'Unblocking',

	'REC' => 'recommend',
	'URE' => 'remove recommendation',

	'WRN' => 'warning',
	'UWN' => 'remove warning',

	'SPA' => 'add stamp',
	'SPD' => 'undo Stamp',

	'SLA' => 'add icon',
	'SLD' => 'undo icon',

	'REG' => 'group recommendation',

	'PTA' => 'generate article',

	'MAG' => 'prop',

	'REB' => 'push',

	'LIV' => 'live',
	'LIC' => 'Cancel live',
    'admin_type_invalid' => 'Sorry, the target classification is invalid',
    'admin_expiration_invalid' => 'Sorry, the validity period is invalid, Valid period should be between{min}and{max}',

    'CNL_STICK'	=>	'cancel topping',
    'CNL_HIGHLIGHE'	=>	'unhighlight',
    'CNL_DIGEST'	=>	'cancel essence',


    'member_manage'			=>			'user management',
    'copy_address'			=>			'copy address',
    'appname'				=>			'Talk Board',
    'settings'				=>			'set',
    'return'				=>			'return',
    'member_manage'			=>			'member management',
    'index'					=>			'home page',
    'stick'					=>			'topping',
    'highlight'				=>			'highlight',
    'digest'				=>			'essence',
    'archive'				=>			'file',
    'anytime'						=>				 'anytime',
	'7_days_ago'					=>				 'last 7 days',
	'30_days_ago'					=>				 'last 30 days',
	'90_days_ago'					=>				 'last 90 days',
	'assign_time'					=>				 'custom time',
	'date_range'					=>				 'time frame',
	'starttime'						=>				 'start time',
	'endtime'						=>				 'end time',
	'need_login'					=>				 'You need to log in first!',
	'favorite_success'				=>				 'Collection succeeded!',
	'favorite_failed'				=>				 'Collection failed!',
	'cancle_favorite_success'		=>				 'Cancel collection successfully!',
	'cancle_favorite_failed'		=>				 'Failed to cancel collection!',
	'delete_success'				=>				 'Deletion succeeded',
	'delete_failed'					=>				 'Delete failed',
	'thg_delete_success'			=>				 'Completely deleted successfully',
	'thg_delete_failed'				=>				 'Complete deletion failed',
	'empty_delete_success'			=>				 'Emptying the Recycle Bin succeeded',
	'empty_delete_failed'			=>				 'Failed to empty the Recycle Bin',
	'recovery_success'				=>				 'recovery was successful',
	'discuss_archived_no_cmt'		=>				 'This discussion board has been archived and can only be commented after recovery',
	'thread_archived_no_cmt'		=>				 'This topic has been archived and can only be commented after recovery',
	'sub_paramet_error'				=>				 'Submit parameter error',
	'please_input_cmt'				=>				 'Please enter comments',
	'comment_success'				=>				 'Comment succeeded',
	'comment_failed'				=>				 'Comment failed',
	'sub_proving_failed'			=>				 'Submit validation failed',
	'discuss_archived_no_del_cmt'	=>				 'This discussion board has been archived. You can delete comments only after restoring it',
	'thread_archived_no_del_cmt'	=>				 'This topic has been archived. You can delete comments only after recovery',
	'thread_no_exist'				=>				 'Topic does not exist or has been deleted',
	'discuss_no_exist'				=>				 'Discussion board does not exist or has been deleted',
	'delete_cmt_success'			=>				 'Successfully deleted the comment',
	'delete_cmt_failed'				=>				 'Failed to delete comment',
	'discuss_archived_cannot_do'	=>				 'The discussion board has been archived. This operation can only be performed after recovery',
	'thread_archived_cannot_do'		=>				 'The subject has been archived. This operation can only be performed after recovery',
	'archived_success'				=>				 'Archiving succeeded',
	'archived_failed'				=>				 'Archive failed',
	'cancle_archive_success'		=>				 'Archive canceled successfully',
	'cancle_archive_failed'			=>				 'Failed to cancel archiving',
	'rename'						=>				 'rename',
	'rename_success'				=>				 'Rename succeeded',
	'rename_failed'					=>				 'Rename failed',
	'member'						=>				 'member',
	'only_appoint_member'			=>				 'Only specified members can create new discussion boards. Please contact the administrator',
	'exceed_max_discuss'			=>				 'Sorry, the maximum number of new discussion boards has been exceeded. Please contact the administrator',
	'create_failed'					=>				 'Creation failed',
	'create_success'				=>				 'Created successfully',
	'create_discuss'				=>				 'Create Discussion Board',
	'need_discuss_member'			=>				 'This discussion board is private. You are not a member of the discussion board and cannot view it',
	'only_member_can_see'			=>				 'This discussion board is set to be accessible only to discussion board members',
	'discuss_delete_success'		=>				 'Discussion board deleted successfully',
	'discuss_delete_failed'			=>				 'Discussion board deletion failed',
	'post_no_exist'					=>				 'The post does not exist or has been deleted',
	'please_chose_discuss'			=>				 'Please select a discussion board',
	'please_chose_board'			=>				 'Please select the post to operate',
	'post_success'					=>				 'Posting succeeded',
	'my_post'						=>				 'My posts',
	'my_favorite'					=>				 'My Collection',
	'recyclebin'					=>				 'recycle bin',
	'undefined_opt'					=>				 'No action defined',
	'no_next_thread'				=>				 'There is no next topic',
	'no_last_thread'				=>				 'No previous topic',
	'search_id_no_exist'			=>				 'Search ID does not exist',
	'search_dis_error'				=>				 'Search section error',
	'basic'							=>				 'Basic settings',
	'manage'						=>				 'Discussion Board Management',
	'cover'							=>				 'Icon management',
	'allow_create_member'			=>				 'Members allowed to create discussion boards',
	'discuss_recovery_success'		=>				 'Discussion board restored successfully',
	'discuss_recovery_failed'		=>				 'Discussion board recovery failed',
	'wxapp_setting'					=>				 'WeChat application settings',
	'wx_setting'					=>				 'WeChat Settings',
	'discuss'						=>				 'talk board',
	'app_desc'						=>				 'Enterprise talk board application, internal communication and discussion.',
	'post'							=>				 'Posts',
	'my'							=>				 'my',
	'plate'							=>				 'plate',
	'set_menu_key'					=>				 'Set menu KEY value',
	'set_menu_redirect'				=>				 'Set menu jump link',
	'send_failed'					=>				 'Publishing failed',
	'send_failed_wx_no_agentid'		=>				 'Failed to publish. The application has not created WeChat agentid',
	'message_will_be_sent_to_there' =>				 'The messages and notifications of the platform will be sent here in a timely manner',
	'reply_post'					=>				 'Reply to posts',
	'send_post'						=>				 'Send Post',
	'edit_post'						=>				 'edit post',
	'published_in'					=>				 'Published on',
	'least_one_class_enabled'		=>				 'When you enable topic classification, you must keep at least one enabled classification',
	'least_a_administrator'			=>				 'At least one administrator is required',
	'add_member'					=>				 'Add Members',
	'opt_failed'					=>				 'operation failed',
	'input_message'					=>				 'Please enter the content!',
	'give_the_discuss_name'			=>				 'Give the talk board a name',
	'chose_icon_color'				=>				 'Select icon color',
	'allow_discuss_share'			=>				 'Allow talk boards to be shared',
	'discuss_rule'					=>				 'talk Board Rules',
	'see_ubb_code'					=>				 'Click to view the UBB code',
	'ubb_code'						=>				 '<li>[b]Bold text Abc[/b]</li><li>[i]Italic text Abc[/i]</li><li>[u]Underlined text Abc[/u]</li><li>[color=red]Red[/color]</li><li>[size=3]Text size is 3[/size]</li><li>[font=仿宋]Font is Song Dynasty[/font]</li><li>[align=Center]内容居中[/align]</li><li>[url]http://www.dzzoffice.com[/url]<br />[url=http://www.dzzoffice.com]dzzoffice[/url]</li><li>[img]http://www.dzzoffice.com/images/logo.png[/img]<br />[img=宽度,高度]http://www.dzzoffice.com/images/logo.png[/img]</li>',
	'thread_class'					=>				 'topic classification',
	'open_thread_class'				=>				 'Enable topic classification',
	'chose_class'					=>				 'Select Classification',
	'please_chose'					=>				 'Please select',
	'opt_description'				=>				 'Operating Instructions',
	'please_chose_class'			=>				 'Please select a category',
	'title_color'					=>				 'Title Color',
	'title_effect'					=>				 'Title effect',
	'term_of_validity'				=>				 'term of validity',
	'one_day'						=>				 'one day',
	'one_week'						=>				 'a week',
	'30_days'						=>				 '30 days',
	'90_days'						=>				 '90 days',
	'custom'						=>				 'custom',
	'time_frame'					=>				 'time frame',
	'endtime_must_gt_starttime'		=>				 'End time must be greater than start time',
	'true_cancel_highlight_status'	=>				 'Are you sure you want to cancel highlighting?',
	'cancel_highlight'				=>				 'Unhighlight',
	'true_cancel_dist_status'		=>				 'Are you sure you want to cancel the essence state?',
	'cancel_dist'					=>				 'Cancel essence',
	'top'							=>				 'top',
	'settop'						=>				 'Topping',
	'settop_type'					=>				 'Topping type',
	'settop_global'					=>				 'Global Top',
	'settop_plate'					=>				 'This board is placed on the top',
	'settop_class'					=>				 'Category topping',
	'class'							=>				 'classification',
	'plate'							=>				 'This board',
	'global'						=>				 'overall situation',
	'true_cancel_settop'			=>				 'Are you sure you want to cancel topping?',
	'cancel_settop'					=>				 'Cancel Topping',
	'reply'							=>				 'reply',
	'more_reply'					=>				 'More replies',
	'more_comment'					=>				 'More comments',
	'true_recovery_thread'			=>				 'Are you sure you want to restore this topic?',
	'true_recovery'					=>				 'Are you sure you want to recover?',
	'cancel_archive'				=>				 'Cancel archiving',
	'true_delete_comment'			=>				 'Are you sure you want to delete this comment?',
	'true_archive_thread'			=>				 'Are you sure you want to archive this topic?',
	'true_delete_thread'			=>				 'Are you sure you want to delete this topic?',
	'archived_thread'				=>				 'Archived Posts',
	'archived_discuss'				=>				 'Archived section',
	'order_archive_time'			=>				 'By archive time',
	'order_discuss_create_time'		=>				 'By Discussion Board Creation Time',
	'order_post_sent_time'			=>				 'By posting time',
	'order_reply_time'				=>				 'By reply time',
	'order_comment_time'			=>				 'By comment time',
	'order_favorite'				=>				 'By Favorite Time',
	'order_heat'					=>				 'By heat',
	'order_essence'					=>				 'According to essence',
	'order_create_time'				=>				 'By Creation Time',
	'no_archive_content'			=>				 'Content not yet archived',
	'thread'						=>				 'topic',
	'post'							=>				 'Posts',
	'archived'						=>				 'Archived',
	'essence'						=>				 'essence',
	'true_cancel_archive_thread'	=>				 'Are you sure you want to cancel archiving this topic?',
	'true_cancel_archive_discuss'	=>				 'Are you sure you want to cancel archiving this discussion board?',
	'send_new_thread'				=>				 'Post a new post',
	'no_post'						=>				 'No posts',
	'moderator'						=>				 'Board owner',
	'favorite'						=>				 'Collection',
	'cancel_favorite'				=>				 'Cancel Collection',
	'archive_discuss'				=>				 'Filing board',
	'pian'							=>				 'piece',
	'true_delete_seleted_thread'	=>				 'Are you sure you want to delete the selected topic?',
	'true_delete_discuss'			=>				 'Are you sure you want to delete this discussion board?',
	'mycreate'						=>				 'I created',
	'print_this_page'				=>				 'Print this page',
	'source'						=>				 'source',
	'fire'							=>				 'fire',
	'print'							=>				 'Print',
	'flashback'						=>				 'Flashback browsing',
	'positive_sequence'				=>				 'Browse in positive order',
	'fast_reply'					=>				 'Quick reply',
	'activity'						=>				 'Activity',
	'return_top'					=>				 'Back to top',
	'anony'							=>				 'Conceal',
	'straight_floor'				=>				 'Direct to the floor',
	'jump_to_the_specified_floor'	=>				 'Jump to the specified floor',
	'show_all_floor'				=>				 'Show all floors',
	'just_look_the_author'			=>				 'Only the author',
	'thread_range'					=>				 'Subject Scope',
	'search_range'					=>				 'Search Scope',
	'create_author'					=>				 'creator',
	'please_enter_name_of_creator'	=>				 'Please enter the creator name',
	'time_range'					=>				 'time frame',
	'my_comment'					=>				 'My comments',
	'received_comments'				=>				 'Comments received',
	'no_comment'					=>				 'No comments yet',
	'no_right_to_view_this_post'	=>				 'The administrator has removed your view permission',
	'de_post'						=>				 's post',
	'favorited_post'				=>				 'Favorite Posts',
	'favorited_discuss'				=>				 'Favorite sections',
	'no_favorite'					=>				 'No favorite content',
	'favorited'						=>				 'Favorite',
	'no_discuss'					=>				 'No discussion board',
	'last_send'						=>				 'Final publication',
	'comment_my_post'				=>				 'Comment on my posts',
	'trash_num_g'					=>				 'common',
	'trash_num_x'					=>				 'term',
	'empty_recycle_bin'				=>				 'Empty Trash',
	'recycle_bin_nothing'			=>				 'The Recycle Bin does not have any content yet',
	'reduction'						=>				 'reduction',
	'deleter'						=>				 'deleter',
	'delete_date'					=>				 'Delete Date',
	'true_empty_recycle_bin'		=>				 'Are you sure you want to empty the Recycle Bin?',
	'true_thoroughly_del'			=>				 'Once deleted, it cannot be recovered. Are you sure you want to delete completely?',
	'each_user_set_max_discuss'		=>				 'Maximum number of discussion boards created by each user',
	'empty_unlimited'				=>				 'Leave blank or 0 is unlimited',
	'index_cache_time'				=>				 'First page cache time',
	'blank_not_cached_need_server_open_cache'			=>				'Unit: seconds. Leave blank or 0 means no cache. The server installation is required to enable the caching function',
	'allow_create_discuss_user'		=>				 'Users allowed to create discussion boards',
	'full_station_user'				=>				 'All users',
	'specified_user'				=>				 'Specify User',
	'opt_the_update_of_views'		=>				 'Optimize and update topic views',
	'opt_the_update_of_views_desc'	=>				 'If Yes is selected, the number of topic posts will not be updated each time. It is recommended to select "Yes".',
	'open_to_prevent_refresh'		=>				 'View data enable anti refresh',
	'open_to_prevent_refresh_desc'	=>				 'If "No" is selected, each time the topic is visited, the number of views will increase by 1, which will increase the pressure on the server. It is recommended to select "Yes"',
	'moderator_permissions'			=>				 'Board owner permission',
	'settop_one'					=>				 'Topping 1 (classified topping)',
	'settop_two'					=>				 'Set top 2 (built-in top of board)',
	'settop_three'					=>				 'Top 3 (global top)',
	'popular_theme_display_level'	=>				 'Display level of hot topics',
	'icon_display'					=>				 'Icon display',
	'fire'							=>				 'hot',
	'help_block'					=>				 '<ul class="help-block"><li>Topics popularity will be calculated according to the number of participants, including replies, browsing topics, collections</li><li>Set the heat index corresponding to each level of the topic icon on the topic list page. The default is 3 levels. Please separate them with commas</li><li>such：“50,100,200”It means that when the heat of the subject is greater than 50, it is level 1, when it is greater than 100, it is level 2, and when it is greater than 200, it is level 3。</li><li>Leave blank to show no icon</li></ul>',
	'manage_operation_reason'		=>				 'Manage operation reason options',
	'this_set_will_be_display_when_manage'			=>				'This setting will be displayed when the user performs some management operations',
	'post_order_unit'				=>				 'Post sequence unit',
	'post_order_unit_desc'			=>				 'Set the unit for displaying the serial number of posts, for example, "#" will be displayed as 1#',
	'post_order_name'				=>				 'Post Sequence Name',
	'post_order_name_desc'			=>				 'Set the sequence name of all posts in each topic, fill in a name in each line, the topic post will display the owner by default, and the first line represents the first reply to the topic, and so on',
	'general_edition_rules'			=>				 'General Board Rules',
	'general_edition_rules_desc'	=>				 'The forum rules displayed on the home page are left blank. Support UBB code',
	'save_change'					=>				 'Save Changes',
	'seleted'						=>				 'Check',
	'page_p'						=>				 'piece',
	'deleted'						=>				 'Deleted',
	'create_in'						=>				 'Created on', 
	'true_thoroughly_delete_discuss'		=>		 'Are you sure you want to completely delete the selected discussion board (this operation cannot be undone)?',
	'new_post'						=>				 'Post a new post',
	'press_share_qrcode'			=>				 'Long press to share QR code',
	'copy_address'					=>				 'Copy address',
	'copy_success'					=>				 'Copy succeeded',
	'copy_failed'					=>				 'Copy failed',
	'true_delete'					=>				 'Are you sure to delete?',
	'create_name_to_discuss'		=>				 'Give the discussion board a name',
	'you_need'						=>				 'You need',
	'talk_about_your_opinion'		=>				 'Talk about your opinion',
	'talk_something'				=>				 'Talk something',
	'send'							=>				 'Send',
	'please_input_reply'			=>				 'Please enter the reply content',
	'nothing'						=>				 'Nothing',
	'please_input_content'			=>				 'Please enter the content',
	'post_title_not_empty'			=>				 'Post title cannot be empty',
	'post_content_too_little'		=>				 'Too few posts!',
	'no_content'					=>				 'No content temporarily',
	'i_commented'					=>				 'I commented',
	'give_title_to_the_post'		=>				 'Give the post a title',
	'most_can_be_upload'			=>				 'Only upload at most',
	'zhang_image'					=>				 'Pictures',
	'image_too_big'					=>				 'Picture is too large',
	'whether_or_not_anonymous'		=>				 'Is it anonymous',
	'mandatory_anonymous_post'		=>				 'This section is mandatory anonymous posting section',
	'search_result'					=>				 'Search result',
	'people'						=>				 'People',
	'see_all_comments'				=>				 'View all comments',
	'result'						=>				 'Result',
	'find'							=>				 'Find',
	'find_thread'					=>				 'Find related topics',
	'related_content'				=>				 'Related Contents',
	'ge'							=>				 'individual',  
	'search_no_result'				=>				 'No results meet the screening criteria at this time',
	'notification'					=>				 'Notification',
	'add_administrator'				=>			     'Add administrator',
	'can_be_input'					=>				 '(You can also enter <b id="checklen">80</b>characters)',
	'please_choose_post_mod'		=>				 'Please select the posting module',
	'please_choose_thread_class'	=>				 'Please select a topic category',
	'anonymous_post'				=>				 'Anonymous posting',
	'release'						=>				 'Release',
	'coercion_anonymity_desc'		=>				 'Mandatory anonymous mode. Posts cannot be edited after being published, and no one can get the information of the sender',	
	'current_content_not_saved'		=>				 'The current content has not been saved. Are you sure you want to leave?',
	'delete_this_discuss'			=>				 'Delete this discussion board',
	'can_be_recover_in_recycle'		=>				 'After deletion, it can be recovered in the recycle bin. Are you sure you want to delete?',
	'discuss_name'					=>				 'Discussion Board Name',
	'extend_set'					=>				 'Extended Settings',
	'share_rights'					=>				 'Sharing permissions',
	'share_rights_desc'				=>				 'The discussion boards allowed to be shared can be accessed through links',
	'publishing_theme_permissions'	=>				 'Publish Topic Permission',
	'publishing_theme_permissions_desc'			=>				'For private discussion boards, only members can view and post topics',
	'reply_permissions'				=>				 'Operation authority',
	'reply_permissions_desc'		=>				 'For private discussion boards, only members can view and reply',
	'not_allow'						=>				 'Not allow',
	'user_selectable'				=>				 'User selectable',
	'mandatory_anonymous'			=>				 'Enforce anonymity', 
	'mandatory_anonymous_desc'		=>				 'Users are optional, but real users are not displayed at the post display; Forced anonymity: user information is not recorded at all, and the administrator cannot obtain the information of the sender', 
	'management_group_special_purpose'			=>				'Dedicated to management group',
	'add_class'						=>				 'Add class',
	'fill_in_the_display_order'		=>				 'Fill in the display order',
	'fill_in_the_classified_name'	=>				 'Fill in classification name',
	'loadmore'						=>				 'Load more',
	'commnet'						=>				 'Comment',
	'send_comment'					=>				 'Send comment',
	'save_ing'						=>				 'Saving...',
	'sure_delete_post_not_be_recover'			=>				'Are you sure you want to delete this reply? (It cannot be restored after deletion)',





    'discuss_thread_moderate_title'=>'Topic management reminder',   
	'discuss_thread_moderate_wx'	=>'{author}Set your topics:{subject}<b>{optitle}</b>', 
	'discuss_thread_moderate_redirecturl'	=>'{url}',                                  

	'discuss_user_add_title'=>'talk board user reminder',
	'discuss_user_add'=>'{author}Set you as a talk board:<a href="{url}">{discussname}</a> the<b>{member}</b>',
	'discuss_user_add_wx'	=>'{author}Set you as a talk board:{discussname}<b>{member}</b>',
	'discuss_user_add_redirecturl'	=>'{url}',
	
	'discuss_user_remove_title'=>'talk board user reminder',
	'discuss_user_remove'=>'{author}Talk board canceled:<a href="{url}">{discussname}</a>&nbsp;Your membership in',
	'discuss_user_remove_wx'	=>'{author}Talk board canceled: {discussname}&nbsp;Your membership in',
	'discuss_user_remove_redirecturl'	=>'{url}',

	'discuss_user_perm_title'=>'Talk board user rights change',
	'discuss_user_perm'=>'{author}Set you as a talk board:<a href="{url}">{discussname}</a>&nbsp;<b>{permtitle}</b>',
	'discuss_user_perm_wx'	=>'{author}Set you as a talk board: {discussname}&nbsp;<b>{permtitle}</b>',
	'discuss_user_perm_redirecturl'	=>'{url}',
	
	'discuss_thread_reply_title'=>'Post reply reminder',
	'discuss_thread_reply'=>'{author}Replied to your post:<a href="{url}">{subject}</a>&nbsp;<b>{message}</b>',
	'discuss_thread_reply_wx'	=>'{author}Replied to your post: {subject}&nbsp;<b>{message}</b>',
	'discuss_thread_reply_redirecturl'	=>'{url}',
	
	'discuss_delete_thread_title'=>'Topic deletion reminder',
	'discuss_delete_thread'=>'{author}Deleted topic:{threadname}, to<a href="{url}">recycle bin</a>',
	'discuss_delete_thread_wx'	=>'{author}Deleted topic: {threadname}',
	'discuss_delete_thread_redirecturl'	=>'{url}',

	'discuss_delete_title'=>'Talk board deletion reminder',
	'discuss_delete'=>'{author}Discussion board deleted:{discussname},to<a href="{url}">recycle bin</a>',
	'discuss_delete_wx'	=>'{author}Talk board deleted: {discussname}',
	'discuss_delete_redirecturl'	=>'{url}',

	'discuss_recovery_title'=>'Talk board recovery reminder',
	'discuss_recovery'=>'{author}Talk board restored:<a href="{url}">{discussname}</a>',
	'discuss_recovery_wx'	=>'{author}Talk board restored: {discussname}',
	'discuss_recovery_redirecturl'	=>'{url}',

	'discuss_thorough_delete_title'=>'Talk board complete deletion reminder',
	'discuss_thorough_delete'=>'{author}Completely deleted the talk board:{discussname}',
	'discuss_thorough_delete_wx'	=>'{author}Completely deleted the talk board: {discussname}',
	'discuss_thorough_delete_redirecturl'	=>'{url}',
	
	'discuss_archive_title'=>'Talk board archiving reminder',
	'discuss_archive'=>'{author}Archived talk board:<a href="{url}">{discussname}</a>',
	'discuss_archive_wx'	=>'{author}Archived talk board: {discussname}',
	'discuss_archive_redirecturl'	=>'{url}',

	'thread_archive_title'=>'Subject archiving reminder',
	'thread_archive'=>'{author}Archived topics:<a href="{url}">{threadname}</a>',
	'thread_archive_wx'	=>'{author}Archived topics: {threadname}',
	'thread_archive_redirecturl'	=>'{url}',

	'thread_restore_title'=>'Cancel subject archiving reminder',
	'thread_restore'=>'{author}Canceled archiving of subject:<a href="{url}">{threadname}</a>',
	'thread_restore_wx'	=>'{author}Cancel subject archiving: {threadname}',
	'thread_restore_redirecturl'	=>'{url}',

	'discuss_restore_title'=>'Cancel the archive reminder of the talk board',
	'discuss_restore'=>'{author}Canceled archiving of talk board:<a href="{url}">{discussname}</a>',
	'discuss_restore_wx'	=>'{author}Cancel talk board archiving: {discussname}',
	'discuss_restore_redirecturl'	=>'{url}',
	
	'discuss_at'=>'{author}@(mentioned)you，<a href="{url}">Go and have a look</a>',
	'discuss_at_wx'=>'{author}@(mentioned)you:{message}',
	'discuss_at_redirecturl'=>'{url}',
	'discuss_at_title'=>'@I remind',

	'discuss_cmt_reply_title'=>'Comment reply reminder',
	'discuss_cmt_reply'=>'{author}Replied to you:{message} <a href="{url}">Go and have a look</a>',
	'discuss_cmt_reply_wx'	=>'{author}Replied to you: <b>{message}</b>',
	'discuss_cmt_reply_redirecturl'	=>'{url}',

	'discuss_cmt_title'=>'Comment reminder',
	'discuss_cmt'=>'{author}Commented on your post:{message} <a href="{url}">Go and have a look</a>',
	'discuss_cmt_wx'	=>'{author}Commented on your post: <b>{message}</b>',
	'discuss_cmt_redirecturl'	=>'{url}'
);

?>
